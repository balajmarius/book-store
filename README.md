# 📚 Book Store
> Book Store application using GraphQL and Apollo

### 🌎 [Live](http://alacritylaw-test.surge.sh/)

### Install dependencies 💉

```bash
npm run install
```
### Develop 🛠

```bash
npm run start
```

### Lint 💅

```bash
npm run lint
```

### Build 👩‍💻

```bash
npm run build
```
