import ApolloClient from 'apollo-boost'
import { InMemoryCache } from 'apollo-cache-inmemory'

// config
import config from '../constants'

const configureStore = (uri=config.graphQL) => {
  
  return new ApolloClient({ uri, cache: new InMemoryCache() })

}

export default configureStore
