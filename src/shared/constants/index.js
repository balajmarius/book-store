const env = process.env.NODE_ENV

const development = {
  graphQL: 'http://localhost:4567/graphql'
}

const production = {
  graphQL: 'https://alacrityapollo-test-app-oruefzgcof.now.sh/graphql'
}

const config = {
  development,
  production
}

export default config[env]