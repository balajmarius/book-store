/* eslint-disable jsx-a11y/accessible-emoji */
import React from 'react'

const Component = ({ message }) => (
  <div className='box box--is-error'>🛑 {message}</div>
)

export default Component
