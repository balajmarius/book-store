/* eslint-disable jsx-a11y/accessible-emoji */
import React from 'react'

const Component = () => (
  <div className='box box--is-loading'>⌛️ Loading..</div>
)

export default Component
