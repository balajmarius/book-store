import React, { Component } from 'react'
import { ApolloProvider } from 'react-apollo'

// store
import configureStore from '../store'

// components
import Router from './router'

// configure store
const client = configureStore()

export default class Root extends Component {

  render() {

    return (
      <div className='wrapper'>
        <ApolloProvider client={client}>
          <Router/>
        </ApolloProvider>
      </div>
    )
    
  }

}