import React, { Component } from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

// components
import List from '../../list'
import Create from '../../create'
import Edit from '../../edit'

export default class Router extends Component {

  render() {

    return (
      <BrowserRouter>
        <Switch>
          <Route path='/' component={List} exact/>
          <Route path='/create' component={Create}/>
          <Route path='/edit' component={Edit}/>
        </Switch>
      </BrowserRouter>
    )

  }

}
