import React from 'react'
import { render } from 'react-dom'

// import css
import './shared/style/main.css'

// components
import Root from './shared/containers/root'

render(<Root/>, document.getElementById('root'))
