/* eslint-disable jsx-a11y/accessible-emoji */
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { withStateHandlers } from 'recompose'
import { Mutation } from 'react-apollo'

// components
import { Success, Error, Spinner } from '../shared/components'

// constants
import { FORM_DEFAULTS, MESSAGES } from './constants'

// mutations
import { createBook } from './mutations'

// query
import { name as booksQuery } from '../list'

class Create extends Component {

  constructor() {

    super()

    this.renderForm = this.renderForm.bind(this)

  }

  renderForm(createBook) {

    const {
      title,
      author,
      price,
      loading,
      setUI } = this.props

    // let user submit if the fields are completed
    const isFormCompleted = Boolean(title && author && price)

    /**
     * @description callback for changing input value
     * @param {String} key - input key in 'reducer'
     * @return {Function} setInput action
     */
    const onChange = key => event => setUI({ [key]: event.target.value })

    /**
     * @description callback for submitting form
     * @param {Object} event 
     */
    const onSubmit = event => {

      // prevent page refresh
      event.preventDefault()

      // prevent submit if form not completed
      if (!isFormCompleted) return

      return Promise.resolve()
        .then(() => setUI({ loading: true, error: null }))
        .then(() => createBook({ variables: { title, price, author } }))
        .then(() => setUI({ ...FORM_DEFAULTS, message: MESSAGES.success }))
        .catch(e => setUI({ error: e.toString(), loading: false }))

    }    

    return (
      <form className='box' onSubmit={onSubmit}>
        <h2 className='title'>📌 Create</h2>

        <input placeholder='Title' type='text' value={title} onChange={onChange('title')} disabled={loading} required/>
        <input placeholder='Author' type='text' value={author} onChange={onChange('author')} disabled={loading} required/>
        <input placeholder='Price' type='number' step='any' value={price} onChange={onChange('price')} min={FORM_DEFAULTS.price} disabled={loading} required/>

        <div className='grid'>
          <button className='button' disabled={!isFormCompleted}>Create</button>
          <Link to='/'>Cancel</Link>
        </div>
      </form>
    )

  }

  render() {

    const {
      renderForm } = this

    const {
      error,
      message,
      loading } = this.props


    // render states
    const renderError = error ? <Error message={error}/> : null
    const renderSpinner = loading ? <Spinner/> : null
    const renderSuccess = message ? <Success message={message}/> : null

    return (
      <div className='form'>
        {renderError}
        {renderSpinner}
        {renderSuccess}

        <Mutation mutation={createBook} refetchQueries={[booksQuery]}>
          {renderForm}      
        </Mutation>
      </div>
    )

  }

}

export default withStateHandlers(
  // create a reducer-like object
  // prevent use of `setState`, use props instead
  () => ({ ...FORM_DEFAULTS }),
  // redux-like actions binded to props
  {
    setUI: () => payload => ({ ...payload })
  }
)(Create)