import { gql } from 'apollo-boost'

export const createBook = gql`
  mutation createBook($title: String!, $author: String!, $price: Float!) {
    createBook(title: $title, author: $author, price: $price) {
      title,
      price,
      author
    }
  }
`
