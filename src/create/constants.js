export const name = 'create'

export const FORM_DEFAULTS = {
  title: '',
  author: '',
  price: 0,
  message: null,
  loading: false,
  error: null
}

export const MESSAGES = {
  success: 'Book created!'
}
