import { gql } from 'apollo-boost'

export const editBook = gql`
  mutation editBook($bookId: Int!, $title: String!, $author: String!, $price: Float!) {
    editBook(bookId: $bookId, title: $title, author: $author, price: $price) {
      bookId,
      title,
      price,
      author
    }
  }
`
