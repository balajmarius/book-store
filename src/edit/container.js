/* eslint-disable jsx-a11y/accessible-emoji */
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { withRouter } from 'react-router'
import { compose, withStateHandlers } from 'recompose'
import { Mutation } from 'react-apollo'

// components
import { Error, Spinner, Success } from '../shared/components'

// constants
import { FORM_DEFAULTS, MESSAGES } from './constants'

// mutations
import { editBook } from './mutations'

// query
import { name as booksQuery } from '../list'

class Edit extends Component {

  constructor() {

    super()

    this.renderForm = this.renderForm.bind(this)

  }

  componentWillMount() {

    const {
      history,
      location,
      setUI } = this.props

    // redirect to home if is trying
    // to access `/edit` directly
    // TODO: query for single book and `bookId` in url
    if (!location.state) return history.replace('/')

    // merge state with current book details
    return setUI({ ...location.state })

  }

  renderForm(editBook) {

    const {
      title,
      author,
      price,
      bookId,
      loading,
      setUI } = this.props

    /**
     * @description callback for changing input value
     * @param {String} key - input key in 'reducer'
     * @return {Function} setInput action
     */
    const onChange = key => event => setUI({ [key]: event.target.value })

    /**
     * @description callback for submitting form
     * @param {Object} event 
     */
    const onSubmit = event => {

      // prevent page refresh
      event.preventDefault()

      return Promise.resolve()
        .then(() => setUI({ loading: true, error: null, message: null }))
        .then(() => editBook({ variables: { bookId, title, price, author } }))
        .then(() => setUI({ loading: false, message: MESSAGES.success }))
        .catch(e => setUI({ error: e.toString(), loading: false }))

    }    

    return (
      <form className='box' onSubmit={onSubmit}>
        <h2 className='title' >📝 Edit</h2>

        <input placeholder='Title' type='text' value={title} onChange={onChange('title')} disabled={loading} required/>
        <input placeholder='Author' type='text' value={author} onChange={onChange('author')} disabled={loading} required/>
        <input placeholder='Price' type='number' step='any' value={price} onChange={onChange('price')} min={FORM_DEFAULTS.price} disabled={loading} required/>

        <div className='grid'>
          <button className='button'>Update</button>
          <Link to='/'>Cancel</Link>
        </div>
      </form>
    )

  }  

  render() {

    const {
      renderForm } = this

    const {
      error,
      message,
      loading } = this.props

    // render states
    const renderError = error ? <Error message={error}/> : null
    const renderSpinner = loading ? <Spinner/> : null
    const renderSuccess = message ? <Success message={message}/> : null

    return (
      <div className='form'>
        {renderError}
        {renderSpinner}
        {renderSuccess}
          
        <Mutation mutation={editBook} refetchQueries={[booksQuery]}>
          {renderForm}      
        </Mutation>
      </div>
    )

  }

}

export default compose(
  withRouter,
  withStateHandlers(
    // create a reducer-like object
    // prevent use of `setState`, use props instead
    () => ({ ...FORM_DEFAULTS }),
    // redux-like actions binded to props
    {
      setUI: () => payload => ({ ...payload })
    })
)(Edit)