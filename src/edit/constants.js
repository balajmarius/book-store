export const name = 'edit'

export const FORM_DEFAULTS = {
  title: '',
  author: '',
  price: 0,
  bookId: 0,
  message: null,
  loading: false,
  error: null
}

export const MESSAGES = {
  success: 'Updated successfully!'
}
