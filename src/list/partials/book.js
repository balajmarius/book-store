/* eslint-disable jsx-a11y/accessible-emoji */
import React from 'react'
import { Link } from 'react-router-dom'

const Component = ({ bookId, title, author, price, isSaved, toggleSave }) => {

  /**
   * @description input callback
   */
  const onChange = () => toggleSave({ price, bookId })

  // computed edit query
  const editQuery = {
    pathname: '/edit',
    state: {
      bookId,
      title,
      author,
      price
    }
  }

  // render label
  const renderCartLabel = isSaved ? '❌ Remove from cart' : '🛒 Add to cart'

  return (
    <div className='box books__item'>      
      <h2><span>#{bookId}</span> {title}</h2>
      <h3>by {author}</h3>

      <div className='grid'>
        <strong>${price}</strong>
        <div className='items__controls'>
          <Link to={editQuery}>📝 Edit</Link>
          <div>
            <input id={`cart-${bookId}`} name='cart' type='checkbox' onChange={onChange}/>
            <label htmlFor={`cart-${bookId}`}>{renderCartLabel}</label>
          </div>          
        </div>
      </div>
    </div>
  )

}

export default Component

