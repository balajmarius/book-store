import React from 'react'

const Component = ({ total, counter }) => {

  return (
    <div className='box books__cart'>
      <h2 className='title'>🛍 {counter} items</h2>

      <div className='cart__total'>
        <small>Total due</small>
        <h2 className='title'>${total}</h2>
      </div>
    </div>
  )

}

export default Component
