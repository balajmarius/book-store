import { gql } from 'apollo-boost'

export const getBooks = gql`
  query books {
    books {
      bookId,
      title,
      price,
      author
    }
  }
`
