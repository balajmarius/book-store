import React, { Component } from 'react'
import { Query } from 'react-apollo'
import { Link } from 'react-router-dom'
import { withStateHandlers } from 'recompose'

// queries
import { getBooks } from './queries'

// constants
import { BOOKS_DEFAULTS } from './constants'

// partials
import * as Partials from './partials'
import { Spinner, Error } from '../shared/components'

class List extends Component {

  constructor() {

    super()
    
    this.renderBooks = this.renderBooks.bind(this)

  }

  renderBooks({ loading, error, data: { books } }) {

    const {
      cart,
      toggleSave } = this.props

    // render spinner
    // when fetching books
    if (loading) return <Spinner/>

    // handle errors
    if (error) return <Error message={error}/>
    
    // render actual books
    return books.map(book => (
      <Partials.book key={book.bookId} {...book} isSaved={cart.includes(book.bookId)} toggleSave={toggleSave}/>))

  }

  render() {

    const { renderBooks } = this

    const {
      total,
      cart } = this.props

    return (
      <div className='books'>        
        <Partials.cart total={total} counter={cart.length}/>        

        <Query query={getBooks}>
          {renderBooks}
        </Query>

        <Link to='/create' className='button'>Create book</Link>
      </div>
    )

  }

}

export default withStateHandlers(
  () => ({ ...BOOKS_DEFAULTS }),
  {
    toggleSave: ({ cart, total }) => ({ bookId, price }) => {
      
      // if `bookId` already in cart
      // substract the price and remove from array
      // else push it and add price
      return cart.includes(bookId) ?
        ({ cart: cart.filter(i => i !== bookId), total: total-price }) :
        ({ cart: [...cart, bookId], total: total+price })

    },
    setUI: () => payload => ({ ...payload })
  }
)(List)
